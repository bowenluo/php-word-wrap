<?php
/**
 * Created by PhpStorm.
 * User: Bowen
 * Date: 23/10/2018
 * Time: 8:04 PM
 */

namespace BowenLuo\Tests\Util;


use PHPUnit\Framework\TestCase;

/**
 * Class UtilTest
 * @package BowenLuo\Tests\Util
 */
class UtilTest extends TestCase
{
    protected function setUp()
    {
        include_once __DIR__.'/../../src/Util/util.php';
    }

    /**
     * @dataProvider textDataProvider
     *
     * @param string $string
     * @param int $length
     * @param string $expected
     * @throws \Exception
     */
    public function testWrap(string $string, int $length, string $expected)
    {
        $result = wrap($string, $length);
        $this->assertEquals($expected, $result);
    }

    /**
     * @return array array([plain_text, length, expected])
     */
    public function textDataProvider()
    {
        return [
            ["", 0, ""],
            ["B", 0, ""],
            ["", 1, ""],
            ["Bowen", 8, "Bowen"],
            ["Bowen", 7, "Bowen"],
            ["Bowen", 6, "Bowen"],
            ["Bowen", 5, "Bowen"],
            ["Bowen", 4, "Bowe\nn"],
            ["Bowen", 3, "Bow\nen"],
            ["Bowen", 2, "Bo\nwe\nn"],
            ["Bowen", 1, "B\no\nw\ne\nn"],
            ["Bowen", 0, ""],
            ["Bowen", -1, ""],
            ["I am a PHP Developer!", 10, "I am a PHP\nDeveloper!"],
            ["I am a PHP Developer!", 9, "I am a\nPHP\nDeveloper\n!"],
            ["I am a PHP Developer!", 8, "I am a\nPHP\nDevelope\nr!"],
            ["I am a PHP Developer!", 7, "I am a\nPHP\nDevelop\ner!"],
            ["I am a PHP Developer!", 6, "I am a\nPHP\nDevelo\nper!"],
            ["I am a PHP Developer!", 5, "I am\na PHP\nDevel\noper!"],
            ["I am a PHP Developer!", 4, "I am\na\nPHP\nDeve\nlope\nr!"],
            ["I am a PHP Developer!", 3, "I\nam\na\nPHP\nDev\nelo\nper\n!"],
            ["I am a PHP Developer!", 2, "I\nam\na\nPH\nP\nDe\nve\nlo\npe\nr!"],
            ["I am a PHP Developer!", 1, "I\na\nm\na\nP\nH\nP\nD\ne\nv\ne\nl\no\np\ne\nr\n!"],
            ["Test one space", 10, "Test one\nspace"],
            ["Test  two  spaces", 10, "Test two\nspaces"],
            ["Test   three   spaces", 10, "Test three\nspaces"],
            ["Test    four    spaces", 10, "Test four\nspaces"],
            ["Test     five     spaces", 10, "Test five\nspaces"],
            [
                "Avocados are a delicious fruit and an ingredient in many dishes, including guacamole. Avocados are unique and come with a variety of health benefits, like lowering your cholesterol and improving your triglyceride levels.",
                30,
                "Avocados are a delicious fruit\nand an ingredient in many\ndishes, including guacamole.\nAvocados are unique and come\nwith a variety of health\nbenefits, like lowering your\ncholesterol and improving your\ntriglyceride levels.",
            ],
            [
                "Amazingly, you can grow this fantastic food with the seed that you would usually just discard.",
                15,
                "Amazingly, you\ncan grow this\nfantastic food\nwith the seed\nthat you would\nusually just\ndiscard.",
            ],
            [
                "Avocados thrive in warm and humid climates and grow in zones 8 through 10.",
                10,
                "Avocados\nthrive in\nwarm and\nhumid\nclimates\nand grow\nin zones 8\nthrough\n10.",
            ],
        ];
    }
}