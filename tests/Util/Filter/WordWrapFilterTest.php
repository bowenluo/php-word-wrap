<?php
/**
 * Created by PhpStorm.
 * User: Bowen
 * Date: 18/10/2018
 * Time: 8:20 PM
 */

namespace BowenLuo\Util\Filter;


use PHPUnit\Framework\TestCase;

/**
 * Class WordWrapFilterTest
 * @package BowenLuo\Util\Filter
 */
class WordWrapFilterTest extends TestCase
{
    /** @var TextFilterInterface */
    private $textFilter;

    protected function setUp()
    {
        $this->textFilter = new WordWrapFilter();
    }

    /**
     * @dataProvider  textDataProvider
     *
     * @param string $string
     * @param string $expected
     */
    public function testFilter(string $string, string $expected)
    {
        $result = $this->textFilter->filter($string);
        $this->assertEquals($expected, $result);
    }


    /**
     * @return array array([textStr],[expected]...)
     */
    function textDataProvider()
    {
        return [
            ['s1 s2 s3', 's1 s2 s3'],
            ['s1 s2  s3  ', 's1 s2 s3 '],
            [' s1                 ss', ' s1 ss'],
            ['I\nLove\nYou', 'I Love You'],
            ['I\n\nHate\n\n\nYou', 'I Hate You'],
            ['I\n \n \n Miss You', 'I Miss You'],
            ['Good\t\n\tBye~', 'Good Bye~'],
        ];
    }
}
