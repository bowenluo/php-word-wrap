<?php
/**
 * Created by PhpStorm.
 * User: Bowen
 * Date: 20/10/2018
 * Time: 3:17 PM
 */

namespace BowenLuo\Util\WordWrap;


use PHPUnit\Framework\TestCase;

/**
 * Class StandardWordWrapUtilTest
 * @package BowenLuo\Util\WordWrap
 */
class StandardWordWrapUtilTest extends TestCase
{
    private $delimiter = "\n";

    /** @var WordWrapInterface */
    private $wordWrap;

    protected function setUp()
    {
        // No filter
        $this->wordWrap = new StandardWordWrapUtil($this->delimiter);
    }

    /**
     * @dataProvider strDataProvider
     *
     * @param string $str
     * @param int $length
     */
    public function testWrap(string $str, int $length)
    {
        $result = $this->wordWrap->wrap($str, $length);
        $expected = wordwrap($str, $length, $this->delimiter, true);
        $this->assertEquals($expected, $result);
    }

    /**
     * @return array array([str,length])
     */
    public function strDataProvider(): array
    {
        return [
            [
                'Hello World',
                5,
            ],
            [
                'Hello World!',
                12,
            ],
            [
                'a, bb, ccc, dddd, eeee, fffff, ggggg, hhhhh.',
                3,
            ],
            [
                'a, bb, ccc, dddd, eeee, fffff, ggggg, hhhhh.',
                4,
            ],
            [
                'New Zealand News – Crime, Politics, Health, Education - NZ Herald',
                10,
            ],
            [
                'Comprehensive up-to-date news coverage, aggregated from sources all over the world by Google News.',
                25,
            ],
            [
                'BBC News provides trusted World and UK news as well as local and regional perspectives. ... The latest global news, sport, weather and documentaries.',
                20,
            ],
            [
                'Latest & Breaking News from New Zealand and around the world from Newshub - your home for NZ, world, sport, politics and entertainment news.',
                30,
            ],
            [
                'We say what people are thinking and cover the issues that get people talking balancing Australian and global moments — from politics to pop culture',
                50,
            ],
            [
                'PHPToday is a website for PHP developers to share useful links, latest news and jobs. Learn something new everyday and find your next dream job!',
                20,
            ],
        ];
    }
}
