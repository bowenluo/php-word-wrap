<?php
/**
 * Created by PhpStorm.
 * User: Bowen
 * Date: 18/10/2018
 * Time: 7:01 PM
 */

namespace BowenLuo\Util\WordWrap;


use BowenLuo\Util\Filter\TextFilterInterface;
use BowenLuo\Util\Filter\WordWrapFilter;
use PHPUnit\Framework\TestCase;

/**
 * Class WordWrapUtilTest
 * @package BowenLuo\Util\WordWrap
 */
class WordWrapUtilTest extends TestCase
{
    /** @var WordWrapInterface */
    private $wordWrap;

    protected function setUp()
    {
        /** @var TextFilterInterface[] $textFilters */
        $textFilters = [new WordWrapFilter()];

        // Be aware of different results when passing single quotes or double quotes delimiter.
        $this->wordWrap = new WordWrapUtil('\n', $textFilters);
//        $this->wordWrap = new WordWrapUtil("\n", $textFilters);
    }

    /**
     * @dataProvider normalDataProvider
     *
     * @param string $string
     * @param int $length
     * @param string $expected
     * @throws \Exception
     */
    public function testWrapNormalData(string $string, int $length, string $expected)
    {
        $result = $this->wordWrap->wrap($string, $length);
        $this->assertEquals($expected, $result);
//        echo $result.PHP_EOL.PHP_EOL;
    }

    /**
     * @return array array([testStr, length, expected])
     */
    public function normalDataProvider()
    {
        return [
            [
                'Hello World',
                5,
                'Hello\nWorld',
            ],
            [
                'Hello World!',
                12,
                'Hello World!',
            ],
            [
                'a, bb, ccc, dddd, eeee, fffff, ggggg, hhhhh.',
                3,
                'a,\nbb,\ncc-\nc,\ndd-\ndd,\nee-\nee,\nff-\nff-\nf,\ngg-\ngg-\ng,\nhh-\nhh-\nh.',
            ],
            [
                'a, bb, ccc, dddd, eeee, fffff, ggggg, hhhhh.',
                4,
                'a,\nbb,\nccc,\nddd-\nd,\neee-\ne,\nfff-\nff,\nggg-\ngg,\nhhh-\nhh.',
            ],
            [
                'New Zealand News – Crime, Politics, Health, Education - NZ Herald',
                10,
                'New\nZealand\nNews –\nCrime,\nPolitics,\nHealth,\nEducation\n- NZ\nHerald',
            ],
            [
                'Comprehensive up-to-date news coverage, aggregated from sources all over the world by Google News.',
                25,
                'Comprehensive up-to-date\nnews coverage, aggregated\nfrom sources all over the\nworld by Google News.',
            ],
            [
                'BBC News provides trusted World and UK news as well as local and regional perspectives. ... The latest global news, sport, weather and documentaries.',
                20,
                'BBC News provides\ntrusted World and UK\nnews as well as\nlocal and regional\nperspectives. ...\nThe latest global\nnews, sport, weather\nand documentaries.',
            ],
            [
                'Latest & Breaking News from New Zealand and around the world from Newshub - your home for NZ, world, sport, politics and entertainment news.',
                30,
                'Latest & Breaking News from\nNew Zealand and around the\nworld from Newshub - your home\nfor NZ, world, sport, politics\nand entertainment news.',
            ],
            [
                'We say what people are thinking and cover the issues that get people talking balancing Australian and global moments — from politics to pop culture',
                50,
                'We say what people are thinking and cover the\nissues that get people talking balancing\nAustralian and global moments — from politics to\npop culture'
            ]
        ];
    }

    /**
     * @dataProvider zeroAndNegativeLengthDataProvider
     *
     * @param string $string
     * @param int $length
     * @throws \Exception
     */
    public function testWrapZeroAndNegativeLength(string $string, int $length)
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->wordWrap->wrap($string, $length);
    }

    /**
     * @return array array([testStr, length, expected])
     */
    public function zeroAndNegativeLengthDataProvider()
    {
        return [
            ['A', 0],
            ['B', -1],
            ['C', -2],
        ];
    }
}
