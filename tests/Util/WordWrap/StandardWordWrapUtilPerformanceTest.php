<?php
/**
 * Created by PhpStorm.
 * User: Bowen
 * Date: 20/10/2018
 * Time: 5:35 PM
 */

namespace BowenLuo\Tests\Util\WordWrap;


use BowenLuo\Util\WordWrap\StandardWordWrapUtil;
use BowenLuo\Util\WordWrap\WordWrapInterface;
use PHPUnit\Framework\TestCase;

class StandardWordWrapUtilPerformanceTest extends TestCase
{
    /** @var float */
    protected $execTimeStart;

    /** @var float */
    protected $execTimeFinish;

    /** @var string */
    private $delimiter = "\n";

    /** @var WordWrapInterface */
    private $wordWrap;

    protected function setUp()
    {
        $this->wordWrap = new StandardWordWrapUtil($this->delimiter);
    }

    public function testPerformance()
    {
        $times = 1000;
        $length = 10;

        echo "Test custom wordwrap\n";
        $this->execStart();
        for ($i = 0; $i <= $times; $i++) {
            $this->wordWrap->wrap($this->data(), $length);
        }
        $this->execFinish();
        $customExecTime = $this->execTimeFinish - $this->execTimeStart;
        echo "Result: {$customExecTime}\n\n";

        // ===================================

        echo "Test build-in wordwrap\n";
        $this->execStart();
        for ($i = 0; $i <= $times; $i++) {
            wordwrap($this->data(), $length);
        }
        $this->execFinish();
        $buildInExecTime = $this->execTimeFinish - $this->execTimeStart;
        echo "Result: {$buildInExecTime}\n\n";

        $ratio = (int)($customExecTime / $buildInExecTime);
        echo "Ratio: $ratio%\n\n";
        // Ratio: 550% ~ 600% (On my laptop)

        $this->assertTrue(true);
    }

    private function data()
    {
        return 'The avocado (Persea americana) is a tree, long thought to have originated in South Central Mexico,[2][3] classified as a member of the flowering plant family Lauraceae.[4] The fruit of the plant, also called an avocado (or avocado pear or alligator pear), is botanically a large berry containing a single large seed known as a "pit" or a "stone".[5]

Avocados are commercially valuable and are cultivated in tropical and Mediterranean climates throughout the world.[4] They have a green-skinned, fleshy body that may be pear-shaped, egg-shaped, or spherical. Commercially, they ripen after harvesting. Avocado trees are partially self-pollinating and are often propagated through grafting to maintain a predictable quality and quantity of the fruit.';
    }

    /**
     * @return float
     */
    public function execStart(): float
    {
        return $this->execTimeStart = microtime(true);
    }

    /**
     * @return float
     */
    public function execFinish(): float
    {
        return $this->execTimeFinish = microtime(true);
    }


}