<?php
/**
 * Created by PhpStorm.
 * User: Bowen
 * Date: 23/10/2018
 * Time: 7:24 PM
 */

/**
 * Wrap a string into new lines when it reaches a specific length
 *
 * @param string $string The input string
 * @param int $length The maximum length of a line
 * @return string The given string wrapped at the specific length
 */
function wrap(string $string, int $length): string
{
    if (strlen($string) == 0 || $length <= 0) {
        return "";
    }

    if (strlen($string) <= $length) {
        return $string;
    }

    // Split words with single space or multi-spaces
    $words = preg_split('/\s+/', $string);

    $delimiter = "\n";
    $space = " ";
    $lines = [];

    $line = "";
    for ($i = 0; $i < count($words); $i++) {
        // Is the first word of the line?
        $is_first_word = strlen($line) == 0;

        if (strlen($line) + ($is_first_word ? 0 : 1) + strlen($words[$i]) <= $length) {
            if (!$is_first_word) {
                // Add a space before the word, if it is not the first word in the line
                $line .= $space;
            }
            // Append a word to a line
            $line .= $words[$i];
        } else {
            if (strlen($line) > 0) {
                // The word cannot be fitted in the line, add it to the $lines array
                $lines[] = $line;
                // Reset the line to an empty string to start a new line
                $line = "";
            }

            if (strlen($words[$i]) <= $length) {
                $line .= $words[$i];
            } else {
                // Break the long word
                $split_words = str_split($words[$i], $length);
                // Replace the long word with split words
                array_splice($words, $i, 1, $split_words);
                // Offset the increment for the next loop
                --$i;
            }
        }

        // Is the last word of the $words array?
        $is_last_word = count($words) - 1 == $i;
        if ($is_last_word) {
            $lines[] = $line;
        }
    }

    return implode($delimiter, $lines);
}