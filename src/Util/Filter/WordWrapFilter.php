<?php
/**
 * Created by PhpStorm.
 * User: Bowen
 * Date: 18/10/2018
 * Time: 8:09 PM
 */

namespace BowenLuo\Util\Filter;


/**
 * Class WordWrapFilter
 * @package Util\Filter
 */
class WordWrapFilter implements TextFilterInterface
{

    /**
     * Filter out some characters in the string
     *
     * @param string $string
     * @return string
     */
    function filter(string $string): string
    {
        $string = preg_replace('/\\\\t+/', ' ', $string);
        $string = preg_replace('/\\\\n+/', ' ', $string);
        $string = preg_replace('/\s+/', ' ', $string);

        return $string;
    }
}