<?php
/**
 * Created by PhpStorm.
 * User: Bowen
 * Date: 18/10/2018
 * Time: 5:56 PM
 */

namespace BowenLuo\Util\Filter;

/**
 * Interface TextFilterInterface
 * @package Util\Filter
 */
interface TextFilterInterface
{
    /**
     * Filter out some characters in the string
     *
     * @param string $string
     * @return string
     */
    function filter(string $string): string;
}