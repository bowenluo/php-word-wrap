<?php
/**
 * Created by PhpStorm.
 * User: Bowen
 * Date: 20/10/2018
 * Time: 3:17 PM
 */

namespace BowenLuo\Util\WordWrap;

use BowenLuo\Util\Filter\TextFilterInterface;

/**
 * Class StandardWordWrapUtil
 * @package BowenLuo\Util\WordWrap
 */
class StandardWordWrapUtil implements WordWrapInterface
{
    /** @var string */
    private $space = ' ';

    /** @var string */
    private $delimiter;

    /** @var TextFilterInterface[] */
    private $textFilters;

    /**
     * StandardWordWrapUtil constructor.
     * @param string $delimiter
     * @param array $textFilters
     */
    public function __construct(string $delimiter = "\n", array $textFilters = [])
    {
        $this->delimiter = $delimiter;
        $this->textFilters = $textFilters;
    }

    /**
     * Wrap a string into new lines when it reaches a specific length
     *
     * @param string $string
     * @param int $length
     * @return string
     */
    function wrap(string $string, int $length): string
    {
        if ($length <= 0) {
            throw new \InvalidArgumentException('Invalid value: Length cannot be zero or a negative number');
        }

        // Return the string directly
        if (strlen($string) <= $length) {
            return $string;
        }

        // Filter out characters by filters sequentially
        foreach ($this->textFilters as $filter) {
            if ($filter instanceof TextFilterInterface) {
                $string = $filter->filter($string);
            }
        }

        $words = explode($this->space, $string);

        $lines = [];
        $lineElements = [];
        foreach ($words as $indexWord => $word) {
            $isLastWord = count($words) == $indexWord + 1;
            if (strlen($word) > $length) {
                $splitWords = str_split($word, $length);
                foreach ($splitWords as $indexSplitWord => $splitWord) {
                    $isLastSplitWord = count($splitWords) == $indexSplitWord + 1;
                    $this->addWordToLine(
                        $lineElements,
                        $splitWord,
                        $length,
                        $isLastWord && $isLastSplitWord,
                        $lines
                    );
                }
            } else {
                $this->addWordToLine($lineElements, $word, $length, $isLastWord, $lines);
            }
        }

        $finalText = "";
        foreach ($lines as $line) {
            $finalText .= implode($this->space, $line).$this->delimiter;
        }
        $finalText = rtrim($finalText, $this->delimiter);

        return $finalText;
    }

    /**
     * Count how many spaces between words
     *
     * @param array $lineElements Words in a line
     * @return int
     */
    private function countLineElementSpaces(array $lineElements): int
    {
        if (count($lineElements) == 0) {
            return 0;
        }

        return (count($lineElements) - 1) * strlen($this->space);
    }

    /**
     * Count the length of the line
     *
     * @param array $lineElements
     * @return int
     */
    private function countLineElementLength(array $lineElements)
    {
        $lineLength = $this->countLineElementSpaces($lineElements);
        foreach ($lineElements as $lineElement) {
            $lineLength += strlen($lineElement);
        }

        return $lineLength;
    }

    /**
     * @param array $lineElements
     * @param string $word
     * @param int $length
     * @param int $isLastWord
     * @param array $lines
     * @return bool
     */
    private function addWordToLine(
        array &$lineElements,
        string $word,
        int $length,
        int $isLastWord,
        array &$lines
    ): bool {
        $lineLength = $this->countLineElementLength($lineElements);
        // No need to add a space before the word, if it is the first word in the line
        $wordLength = $lineLength == 0 ? strlen($word) : strlen($this->space) + strlen($word);

        if ($lineLength + $wordLength <= $length) {
            $lineElements[] = $word;

            if ($isLastWord) {
                $this->addLineElementsToLines($lineElements, $lines);
            }

            return true;
        }
        $this->addLineElementsToLines($lineElements, $lines);

        return $this->addWordToLine($lineElements, $word, $length, $isLastWord, $lines);
    }

    /**
     * @param array $lineElements
     * @param array $lines
     */
    private function addLineElementsToLines(array &$lineElements, array &$lines)
    {
        // Add line elements to lines array
        $lines[] = $lineElements;

        // Reset line elements
        $lineElements = [];
    }
}