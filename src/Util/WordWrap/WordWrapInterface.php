<?php
/**
 * Created by PhpStorm.
 * User: Bowen
 * Date: 18/10/2018
 * Time: 5:51 PM
 */

namespace BowenLuo\Util\WordWrap;

/**
 * Interface WordWrapInterface
 * @package BowenLuo\Util\WordWrap
 */
interface WordWrapInterface
{
    /**
     * Wrap a string into new lines when it reaches a specific length
     *
     * @param string $string
     * @param int $length
     * @return string
     */
    function wrap(string $string, int $length): string;
}