<?php
/**
 * Created by PhpStorm.
 * User: Bowen
 * Date: 18/10/2018
 * Time: 6:36 PM
 */

namespace BowenLuo\Util\WordWrap;


use BowenLuo\Util\Filter\TextFilterInterface;

/**
 * Class WordWrapUtil
 * @package Util\WordWrap
 */
class WordWrapUtil implements WordWrapInterface
{
    /** @var string */
    private $hyphen = '-';

    /** @var string[] */
    private $signs = [',', '.', ';'];

    /** @var string */
    private $delimiter;

    /** @var TextFilterInterface[] */
    private $textFilters;

    /**
     * WordWrapUtil constructor.
     * @param string $delimiter
     * @param TextFilterInterface[] $textFilters
     */
    public function __construct(string $delimiter = "\n", array $textFilters = [])
    {
        $this->delimiter = $delimiter;
        $this->textFilters = $textFilters;
    }

    /**
     * Wrap a string into new lines when it reaches a specific length
     *
     * @param string $string
     * @param int $length A positive number
     * @return string
     * @throws \Exception
     */
    function wrap(string $string, int $length): string
    {
        if ($length <= 0) {
            throw new \InvalidArgumentException('Invalid value: Length cannot be zero or a negative number');
        }

        // Returns the string directly
        if (strlen($string) <= $length) {
            return $string;
        }

        // Filter out characters by filters sequentially
        foreach ($this->textFilters as $filter) {
            if ($filter instanceof TextFilterInterface) {
                $string = $filter->filter($string);
            }
        }

        $space = ' ';
        $words = explode($space, $string);

        $lines = [];
        $tempLine = '';
        foreach ($words as $indexWord => $word) {
            if (strlen($word) + strlen($tempLine) + (strlen($tempLine) != 0 ? strlen($space) : 0) <= $length) {
                $tempLine .= $space.$word;
            } else {
                if (strlen($tempLine) > 0) {
                    // Add lines array
                    $lines[] = $tempLine;

                    // Reset $tempLine after it has been added to $lines
                    $tempLine = '';
                }

                if (strlen($word) <= $length) {
                    $tempLine = $space.$word;
                } else {
                    // One word cannot be fitted in one line, break it in half or X
                    $splitWords = str_split($word, $length - 1);

                    foreach ($splitWords as $indexSplitWord => $splitWord) {
                        // It will not add the hyphen at the last part of the word
                        if ($indexSplitWord != count($splitWords) - 1) {
                            $splitWord .= $this->hyphen;
                        }

                        // Last loop, dealing with spacial cases
                        if ($indexSplitWord == count($splitWords) - 1) {
                            /**
                             * Special case 1:
                             *  If the last part of the split words is sign
                             *  and the last char in the last lines array is hyphen,
                             *  then replace the hyphen with the sign.
                             *
                             *  For Example: $string='bbbb, ' $length=3
                             *  ---                 ---
                             *  bb-                 bb-
                             *  bb-  should be =>   bb,
                             *  ,
                             *
                             */
                            if (strlen($splitWord) == 1 && $this->containSign($splitWord)) {
                                if (isset($lines[count($lines) - 1])) {
                                    $lastLine = &$lines[count($lines) - 1];

                                    if (substr($lastLine, -1) != $this->hyphen) {
                                        // If the last char is not hyphen, skip the rest of executions
                                        continue;
                                    }

                                    $lastLine = substr_replace(
                                        $lastLine,
                                        $splitWord,
                                        -1,
                                        1
                                    );

                                    // The last split word has been added, break the iteration of the loop
                                    break;
                                }
                            }
                        }
                        $lines[] = $splitWord;
                    }
                }
            }

            // Remove the first space
            $tempLine = ltrim($tempLine, $space);

            // Add the last word to $lines in the last loop
            if ($indexWord == count($words) - 1 && strlen($tempLine) > 0) {
                $lines[] = $tempLine;
            }
        }

        return implode($this->delimiter, $lines);
    }

    /**
     * Detect whether the word contains a sign at the end.
     *
     * @param string $word A word to be checked
     * @return bool Returns true if it contains, otherwise returns false
     */
    private function containSign(string $word): bool
    {
        if (strlen($word) > 0) {
            $lastChar = substr($word, -1);
            if (in_array($lastChar, $this->signs)) {
                return true;
            }
        }

        return false;
    }


    /**
     * @return string
     */
    public function getHyphen(): string
    {
        return $this->hyphen;
    }

    /**
     * @param string $hyphen
     */
    public function setHyphen(string $hyphen): void
    {
        $this->hyphen = $hyphen;
    }

    /**
     * @return string
     */
    public function getDelimiter(): string
    {
        return $this->delimiter;
    }

    /**
     * @param string $delimiter
     */
    public function setDelimiter(string $delimiter): void
    {
        $this->delimiter = $delimiter;
    }

    /**
     * @return TextFilterInterface[]
     */
    public function getTextFilters(): array
    {
        return $this->textFilters;
    }

    /**
     * @param TextFilterInterface[] $textFilters
     */
    public function setTextFilters(array $textFilters): void
    {
        $this->textFilters = $textFilters;
    }
}