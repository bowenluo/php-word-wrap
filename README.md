# PHP Word Wrap

Wrap a string into new lines when it reaches a specific length.

## Getting Started

### Installing
```bash
php composer.phar install
```

### Usage Example
```php
$textFilters = [new WordWrapFilter()];
$wordWrap = new WordWrapUtil('\n', $textFilters);
$result = $wordWrap->wrap('Hello World', 5); 
// Result: Hello\nWorld
```
or
```php
require_once('/Util/util.php');
$result = wrap('Hello World', 5);
// Result: Hello\nWorld
```

### Running the tests
```bash
php phpunit.phar --configuration phpunit.xml --coverage-text --colors=never
```